# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "nvevg-jekyll-theme"
  spec.version       = "0.1.0"
  spec.authors       = ["Vladislav Nikolaev"]
  spec.email         = ["nvladislav9@gmail.com"]

  spec.summary       = "A minimalist Jekyll theme suitable for software developer's blog"
  spec.homepage      = "https://nvevg.me/"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README|_config\.yml)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.1"
end
