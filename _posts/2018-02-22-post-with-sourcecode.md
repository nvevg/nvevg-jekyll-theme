---
layout: post
---
Hello! This is my first post!

{% highlight cpp %}
int main(int argc, char **argv)
{
    return 0;
}
{% endhighlight %}

Vimscirpt:
{% highlight viml linenos %}
set number
set clipboard+=unnamed
{% endhighlight %}
